### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	上午进行了Code Review，老师指出我没有理解Mock的含义，导致代码有无用操作，是大扣分项。今天一开始学习了SQL Basic为后面的内容做铺垫，然后学习了使用JPA将Spring Boot连接到MySQL数据库并进行基础练习，并学习了使用注解来方便编写代码。

### R (Reflective): Please use one word to express your feelings about today's class.

​	充实

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	今天学习的内容总的来说不算困难，在昨天学习的基础之上连接MySQL数据库，编写Service层并且进行测试。在连接数据库的场景下进行编码学习让我对项目开发有了不同的认识。

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	一定要认真跟着老师的节奏走，不要停留在之前讲的内容。老师讲解的内容和经验一定比自己胡思乱想有用的多！
